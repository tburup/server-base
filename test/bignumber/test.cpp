#include <iostream>
#include <cstring>
#include "Auth/BigNumber.h"

int main()
{
    BigNumber a(324);
    BigNumber b(445);
    
    BigNumber c = a + b;

    if (c.AsDword() != 324+445)
    {
        std::cerr << "addition failed" << std::endl;
        return 1;
    }
    
    c = a * b;
    if (c.AsDword() != 324*445)
    {
        std::cerr << "multiplication failed" << std::endl;
        return 2;
    }
    
    uint8 prettybig[1500];
    std::memset(prettybig, 0, 1500);
    c.SetBinary(prettybig, 1500);
    if (c.AsDword() != 0)
    {
        std::cerr << "big setbinary failed" << std::endl;
        return 3;
    }

    return 0;
}
